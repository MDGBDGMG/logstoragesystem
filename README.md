# LogStorageSystem

利用SparkStreaming，从kafka获取数据，存储到HDFS中。
具体功能：
1.自定义文件存储路径。
2.自定义单个文件大小。
3.每天自动生成一个文件夹，存储当天的日志。
4.可创建Hive外部表访问某天的日志信息。